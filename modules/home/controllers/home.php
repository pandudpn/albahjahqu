<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class home extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('references/reference_model', 'reference');
        setlocale(LC_TIME, 'id_ID');

        $this->check_login();
    }

    public function index(){
        // var_dump($this->session->userdata('user'));die;
    	$this->template->build('index');
    }

}