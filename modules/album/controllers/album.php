<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class album extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('album/album_model', 'album');

        $this->check_login();
    }

    public function index(){
    	$this->template->set('alert', $this->session->flashdata('alert'))
                        ->build('index');
    }

    public function add()
    {
        $this->template
            ->set('alert', $this->session->flashdata('alert'))
            ->set('title', 'New Album')
            ->build('form');
    }

    public function edit($id)
    {
        $is_exist = $this->album->find($id);

        if($is_exist){
            $album  = $is_exist;

            $this->template
                ->set('alert', $this->session->flashdata('alert'))
                ->set('title', 'Edit Album')
                ->set('data', $album)
                ->build('form');
        }
    }

    public function save()
    {
        $id         = $this->input->post('id');
        $title      = $this->input->post('title');
        $type       = 'album';

        $data = array(
            'title' => $title,
            'type'  => $type
        );

        if(!empty($_FILES['image']['name']))
        {
            $config['upload_path']      = './data/images/album';
            $config['allowed_types']    = '*';
            $config['max_size']         = 1024;
            $config['encrypt_name']     = true;
            
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('image')) {
                
            } else {
                $file = $this->upload->data();
                $data['image'] = site_url().'data/images/album/'.$file['file_name'];
            }
        }else{
            $data['image']     = $this->input->post('old_image');
        }
        
        if(!$id)
        {
            $insert = $this->album->insert($data);

            redirect(site_url('album'), 'refresh');
        }
        else
        {
            $update = $this->album->update($id, $data);
            redirect(site_url('album'), 'refresh');
        }
    }

    public function delete($id)
    {
        $data = $this->album->find_by(array('id' => $id, 'deleted' => '0'));

        if(!$id || !$data)
        {
            $error = 'Error: data failed';
            $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

            redirect(site_url('album'), 'refresh');
        }
        else
        {
            $delete = $this->album->set_soft_deletes(TRUE);
            $delete = $this->album->delete($id);

            if($delete)
            {
                $msg = 'delete success';
                $this->session->set_flashdata('alert', array('type' => 'success', 'msg' => $msg));

                redirect(site_url('album'), 'refresh');
            }
            else
            {
                $error = 'Error: something error while deleting';
                $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

                redirect(site_url('album'), 'refresh');
            }
        }
    }

    public function datatables()
    {
        $list = $this->album->get_datatables();
        
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $l->title;
            $row[] = '<div style="width: 75px; height: 75px;"><img style="width: 100%; height: 100%; object-fit: contain" src="'.$l->image.'"></div>';

            //button edit & delete
            $btn   = '<a href="'.site_url('album/edit/'.$l->id).'" class="btn btn-success btn-sm">
                        <i class="fa fa-pencil"></i>
                      </a> &nbsp;';

            $btn  .= '<a href="#" onclick="alert_delete(\''.site_url('album/delete/'.$l->id).'\')" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash"></i>
                      </a>';

            $row[] = $btn;

            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->album->count_all(),
            "recordsFiltered"   => $this->album->count_filtered(),
            "data"              => $data,
        );
        //output to json format
        echo json_encode($output);
    }

}