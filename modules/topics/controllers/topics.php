<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class topics extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('topics/topics_model', 'topics');

        $this->check_login();
    }

    public function index(){
    	$this->template->set('alert', $this->session->flashdata('alert'))
                        ->build('index');
    }

    public function add()
    {
        $this->template
            ->set('alert', $this->session->flashdata('alert'))
            ->set('title', 'New Topic')
            ->build('form');
    }

    public function edit($id)
    {
        $is_exist = $this->topics->find($id);

        if($is_exist){
            $topic  = $is_exist;

            $this->template
                ->set('alert', $this->session->flashdata('alert'))
                ->set('title', 'Edit Topic')
                ->set('data', $topic)
                ->build('form');
        }
    }

    public function save()
    {
        $id         = $this->input->post('id');
        $name       = $this->input->post('name');
        $status     = $this->input->post('status');

        $data = array(
            'name'      => $name,
            'status'    => $status
        );
        
        if(!$id)
        {
            $insert = $this->topics->insert($data);

            redirect(site_url('topics'), 'refresh');
        }
        else
        {
            $update = $this->topics->update($id, $data);
            redirect(site_url('topics'), 'refresh');
        }
    }

    public function delete($id)
    {
        $data = $this->topics->find_by(array('id' => $id, 'deleted' => '0'));

        if(!$id || !$data)
        {
            $error = 'Error: data failed';
            $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

            redirect(site_url('topics'), 'refresh');
        }
        else
        {
            $delete = $this->topics->set_soft_deletes(TRUE);
            $delete = $this->topics->delete($id);

            if($delete)
            {
                $msg = 'delete success';
                $this->session->set_flashdata('alert', array('type' => 'success', 'msg' => $msg));

                redirect(site_url('topics'), 'refresh');
            }
            else
            {
                $error = 'Error: something error while deleting';
                $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

                redirect(site_url('topics'), 'refresh');
            }
        }
    }

    public function datatables()
    {
        $list = $this->topics->get_datatables();

        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $l->name;
            if($l->status == 'disabled'){
                $status = '<span class="badge badge-danger">Disabled</span>';
            }elseif($l->status == 'enabled'){
                $status = '<span class="badge badge-success">Enabled</span>';
            }
            $row[]  = $status;

            //button edit & delete
            $btn   = '<a href="'.site_url('topics/edit/'.$l->id).'" class="btn btn-success btn-sm">
                        <i class="fa fa-pencil"></i>
                      </a> &nbsp;';

            $btn  .= '<a href="#" onclick="alert_delete(\''.site_url('topics/delete/'.$l->id).'\')" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash"></i>
                      </a>';

            $row[] = $btn;

            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->topics->count_all(),
            "recordsFiltered"   => $this->topics->count_filtered(),
            "data"              => $data,
        );
        //output to json format
        echo json_encode($output);
    }

}