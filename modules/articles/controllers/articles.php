<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class articles extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('articles/article_model', 'article');
        $this->load->model('articles/ct_model', 'ct');
        $this->load->model('topics/topics_model', 'topic');

        $this->check_login();
    }

    public function index(){
    	$this->template->set('alert', $this->session->flashdata('alert'))
                        ->build('index');
    }

    public function add()
    {
        $topic  = $this->topic->find_all_by(array('deleted' => 0));
        $this->template
            ->set('alert', $this->session->flashdata('alert'))
            ->set('title', 'New Topic')
            ->set('topic', $topic)
            ->build('form');
    }

    public function edit($id)
    {
        $is_exist = $this->article->find($id);
        $ct       = $this->article->get_by($id);
        
        $topic    = $this->topic->find_all_by(['deleted' => 0]);

        if($is_exist){
            $article  = $is_exist;

            $this->template
                ->set('alert', $this->session->flashdata('alert'))
                ->set('title', 'Edit Article')
                ->set('data', $article)
                ->set('ct', $ct)
                ->set('topic', $topic)
                ->build('form');
        }
    }

    public function save()
    {
        $id             = $this->input->post('id');
        $title          = $this->input->post('title');
        $desc           = $this->input->post('content');
        $type           = 'articles';

        $data = array(
            'title'         => $title,
            'description'   => $desc,
            'type'          => $type
        );

        if(!empty($_FILES['image']['name']))
        {
            $config['upload_path']      = './data/images/articles';
            $config['allowed_types']    = '*';
            $config['max_size']         = 1024;
            $config['encrypt_name']     = true;
            
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('image')) {
                
            } else {
                $file = $this->upload->data();
                $data['image'] = site_url().'data/images/articles/'.$file['file_name'];
            }
        }else{
            $data['image']     = $this->input->post('old_image');
        }
        
        if(!$id)
        {
            $insert = $this->article->insert($data);
            // $last_id= $this->db->insert_id();

            foreach($this->input->post('topic') AS $key){
                $in = [
                    'content_id'    => $insert,
                    'topic_id'      => $key,
                    'type'          => 'articles'
                ];
                $this->ct->insert($in);
            }

            redirect(site_url('articles'), 'refresh');
        }
        else
        {
            $update = $this->article->update($id, $data);
            redirect(site_url('articles'), 'refresh');
        }
    }

    public function delete($id)
    {
        $data = $this->article->find_by(array('id' => $id, 'deleted' => '0'));

        if(!$id || !$data)
        {
            $error = 'Error: data failed';
            $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

            redirect(site_url('article'), 'refresh');
        }
        else
        {
            $delete = $this->article->set_soft_deletes(TRUE);
            $delete = $this->article->delete($id);
            $del    = $this->db->delete('contents_topics', ['content_id' => $id]);

            if($delete)
            {
                $msg = 'delete success';
                $this->session->set_flashdata('alert', array('type' => 'success', 'msg' => $msg));

                redirect(site_url('articles'), 'refresh');
            }
            else
            {
                $error = 'Error: something error while deleting';
                $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

                redirect(site_url('artilces'), 'refresh');
            }
        }
    }

    public function datatables()
    {
        $list = $this->article->get_datatables();
        $cek = $this->article->datatables();
        // echo "<pre>";
        // print_r($list);
        // echo "</pre>";
        // die;
        
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();
            $topic = array();
            
            foreach($cek AS $d){
                if($l->content_id == $d->content_id){
                    $topic[]  = $d->name;
                }
            }
            $row[] = $no;
            $row[] = $topic;
            $row[] = $l->title;
            $row[] = substr(strip_tags($l->description), 0, 200);
            $row[] = date('d M, Y H:i', strtotime($l->created_on));

            //button edit & delete
            $btn   = '<a href="'.site_url('articles/edit/'.$l->content_id).'" class="btn btn-success btn-sm">
                        <i class="fa fa-pencil"></i>
                      </a> &nbsp;';

            $btn  .= '<a href="#" onclick="alert_delete(\''.site_url('articles/delete/'.$l->content_id).'\')" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash"></i>
                      </a>';

            $row[] = $btn;

            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->article->count_all(),
            "recordsFiltered"   => $this->article->count_filtered(),
            "data"              => $data,
        );

        //output to json format
        echo json_encode($output);
    }

}