<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class article_model extends MY_Model {

    protected $table         = 'contents';
    protected $tableCT       = 'contents_topics';
    protected $tableTopic    = 'topics';
    protected $key           = 'id';
    protected $date_format   = 'datetime';
    protected $set_created   = true;

    protected $column_order  = array(null,'title', 'name'); //set column field database for datatable orderable
    protected $column_search = array('contents.title', 'contents.description', 'topics.name', 'contents.created_on'); //set column field database for datatable searchable 
    protected $order         = array('contents.created_on' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by($id){
        $this->db->select($this->table.'.*');
        $this->db->select($this->tableCT.'.topic_id');
        $this->db->from($this->table);
        $this->db->join($this->tableCT, $this->tableCT.'.content_id = '.$this->table.'.id');
        $this->db->join($this->tableTopic, $this->tableTopic.'.id = '.$this->tableCT.'.topic_id', 'left');
        $this->db->where($this->table.'.type', 'articles');
        $this->db->where($this->tableCT.'.content_id', $id);
        return $this->db->get()->result();
    }

    public function _get_datatables_query()
    {
        $this->db->select('*');
        $this->db->select($this->tableTopic.'.name');
        $this->db->from($this->table);
        $this->db->join($this->tableCT, $this->tableCT.'.content_id = '.$this->table.'.id');
        $this->db->join($this->tableTopic, $this->tableTopic.'.id = '.$this->tableCT.'.topic_id', 'left');
        $this->db->where($this->table.'.type', 'articles');
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->open_bracket(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like_not_and($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->close_bracket(); //close bracket
            }
            $i++;
        }

        //deleted = 0
        $this->db->where($this->table. '.deleted', '0');
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        $this->db->group_by($this->table.'.id');
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function datatables(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        $this->db->where($this->table.'.deleted', '0');
        return $this->db->count_all_results();
    }

}