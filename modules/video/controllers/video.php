<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class video extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('video/video_model', 'video');

        $this->check_login();
    }

    public function index(){
    	$this->template->set('alert', $this->session->flashdata('alert'))
                        ->build('index');
    }

    public function add()
    {
        $this->template
            ->set('alert', $this->session->flashdata('alert'))
            ->set('title', 'New Video')
            ->build('form');
    }

    public function edit($id)
    {
        $is_exist = $this->video->find($id);

        if($is_exist){
            $video  = $is_exist;

            $this->template
                ->set('alert', $this->session->flashdata('alert'))
                ->set('title', 'Edit Video')
                ->set('data', $video)
                ->build('form');
        }
    }

    public function save()
    {
        $id         = $this->input->post('id');
        $title      = $this->input->post('title');
        $desc       = $this->input->post('description');
        $type       = 'video';
        $url        = $this->input->post('url');

        $data = array(
            'title'         => $title,
            'description'   => $desc,
            'type'          => $type,
            'url_video'     => $url
        );
        
        if(!$id)
        {
            $insert = $this->video->insert($data);

            redirect(site_url('video'), 'refresh');
        }
        else
        {
            $update = $this->video->update($id, $data);
            redirect(site_url('video'), 'refresh');
        }
    }

    public function delete($id)
    {
        $data = $this->video->find_by(array('id' => $id, 'deleted' => '0'));

        if(!$id || !$data)
        {
            $error = 'Error: data failed';
            $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

            redirect(site_url('video'), 'refresh');
        }
        else
        {
            $delete = $this->video->set_soft_deletes(TRUE);
            $delete = $this->video->delete($id);

            if($delete)
            {
                $msg = 'delete success';
                $this->session->set_flashdata('alert', array('type' => 'success', 'msg' => $msg));

                redirect(site_url('video'), 'refresh');
            }
            else
            {
                $error = 'Error: something error while deleting';
                $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

                redirect(site_url('video'), 'refresh');
            }
        }
    }

    public function datatables()
    {
        $list = $this->video->get_datatables();
        
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $l->title;
            $row[] = $l->description;
            $row[] = "<a href='".$l->url_video."' target='_blank'>".$l->url_video."</a>";

            //button edit & delete
            $btn   = '<a href="'.site_url('video/edit/'.$l->id).'" class="btn btn-success btn-sm">
                        <i class="fa fa-pencil"></i>
                      </a> &nbsp;';

            $btn  .= '<a href="#" onclick="alert_delete(\''.site_url('video/delete/'.$l->id).'\')" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash"></i>
                      </a>';

            $row[] = $btn;

            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->video->count_all(),
            "recordsFiltered"   => $this->video->count_filtered(),
            "data"              => $data,
        );
        //output to json format
        echo json_encode($output);
    }

}