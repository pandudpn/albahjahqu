<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class admin extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('admin/admin_model', 'admin');

        $this->check_login();
    }

    public function index($id=null){

    	$this->template->set('alert', $this->session->flashdata('alert'))
                        ->build('admin');
    }

    public function add()
    {
        $this->template
            ->set('alert', $this->session->flashdata('alert'))
            ->set('title', 'Add User Admin')
            ->build('form');
    }

    public function edit($id)
    {
        $is_exist = $this->admin->find($id);

        if($is_exist){
            $user_admin = $is_exist;

            $this->template
                ->set('alert', $this->session->flashdata('alert'))
                ->set('title', 'Edit User admin')
                ->set('data', $user_admin)
                ->build('form');
        }
    }

    public function save()
    {
        $id         = $this->input->post('id');
        $password   = $this->input->post('password');
        $name       = $this->input->post('name');
        $email      = strtolower($this->input->post('email'));

        $data = array(
            'name'      => $name,
            'email'     => $email
        );
        
        if(!$id)
        {
            $data['password']   = sha1($password.$this->config->item('password_salt'));
            $insert             = $this->admin->insert($data);

            redirect(site_url('admin'), 'refresh');
        }
        else
        {
            if(!empty($password))
            {
                $data['password'] = sha1($password.$this->config->item('password_salt'));
            }

            $update = $this->admin->update($id, $data);
            redirect(site_url('admin'), 'refresh');
        }
    }

    public function delete($id)
    {
        $data = $this->admin->find_by(array('id' => $id, 'deleted' => '0'));

        if(!$id || !$data)
        {
            $error = 'Error: data failed';
            $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

            redirect(site_url('admin'), 'refresh');
        }
        else
        {
            $delete = $this->admin->set_soft_deletes(TRUE);
            $delete = $this->admin->delete($id);

            if($delete)
            {
                $msg = 'delete success';
                $this->session->set_flashdata('alert', array('type' => 'success', 'msg' => $msg));

                redirect(site_url('admin'), 'refresh');
            }
            else
            {
                $error = 'Error: something error while deleting';
                $this->session->set_flashdata('alert', array('type' => 'danger', 'msg' => $error));

                redirect(site_url('admin'), 'refresh');
            }
        }
    }

    public function datatables()
    {
        $list = $this->admin->get_datatables();
        // $list   = $this->admin->find_all();
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $l) {
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $l->name;
            $row[] = $l->email;

            //button edit & delete
            $btn   = '<a href="'.site_url('admin/edit/'.$l->id).'" class="btn btn-success btn-sm">
                        <i class="fa fa-pencil"></i>
                      </a> &nbsp;';

            $btn  .= '<a href="#" onclick="alert_delete(\''.site_url('admin/delete/'.$l->id).'\')" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash"></i>
                      </a>';

            $row[] = $btn;

            $data[] = $row;
        }
 
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->admin->count_all(),
            "recordsFiltered"   => $this->admin->count_filtered(),
            "data"              => $data,
        );
        //output to json format
        echo json_encode($output);
    }

}